# Demo api secured
  * Este projeto foi desenvolvido em java 8, ultilizando Spring Boot Framework versão 2.1.9.RELEASE 

## REQUIREMENTS:
       * Java 8
       * Maven 3.6.2
       * Keycloak (Sin sign-on)
## FEATURES   
	### SERVICES PADRÂO REST
	    - REALIZA COMUNICAÇÂO ENTRE O CLIENTE E O SERVIDOR, FORNECENDO INFORMAÇÔES DOS CLIENTES
	   
	### SWAGGER
	    - documentaion ( http://<IP_SERVER>:8080/swagger-ui.html )
	 
	### Authentication jwt
	    - SSO ( keycloack ) 
          
## RUN PRODUCTION
	
	### MAVEN COMMAND BUILD
	    * mvn clean install
	    
	### MAVEN COMMAND BUILD NO RUN TESTS
	    * mvn clean install -DskipTests
	    
	### DOCKER
	    
	    * BUILD
	    $ docker build -t secured-service .
	    
	    * RUN
	    $ docker run -it secured-service
	
	### DOCKER COMPOSE
	    
	    * BUILD more RUN
	    $ docker-compose up -d --build
 