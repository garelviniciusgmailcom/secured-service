package com.service.demo.api.responses;

import lombok.Data;

@Data
public class Response<T> {

    private String message;
    private T datas;

    public Response(T datas ){
        this.datas = datas;
    }
}
