package com.service.demo.api;

import lombok.Data;

@Data
public class ServiceDTO {

    private String id;
    private String serviceName;
    private String date;

    public ServiceDTO(String id, String serviceName, String date) {
        this.id = id;
        this.serviceName = serviceName;
        this.date = date;
    }
}
