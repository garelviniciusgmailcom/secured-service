package com.service.demo.api.controllers;

import com.service.demo.api.ServiceDTO;
import com.service.demo.api.responses.Response;
import io.swagger.annotations.ApiOperation;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/customers")
public class DemoController {
	
	private final Logger log = LoggerFactory.getLogger(DemoController.class);

	/** GET FIELDS */
	@CrossOrigin
    @GetMapping(value = "/GET/fields", produces = "application/json")
    @ApiOperation("Retorna dados demostrativo!")
	public ResponseEntity<Response<ServiceDTO>> getFilds() {
		this.log.info( "Service: DemoController.getFields() inicializado." );
		ServiceDTO serviceDTO = new ServiceDTO(
				UUID.randomUUID().toString(),
				"secured-service",
				DateTime.now().toString()
		);
		return ResponseEntity.ok(new Response(serviceDTO));
	}

	/** POST ALL */
	@CrossOrigin
    @PostMapping(value = "/POST/all", produces = "application/json")
    @ApiOperation("Retorna registro demostrativo!")
	public ResponseEntity<Response<List<ServiceDTO>>> getAll() {
		this.log.info("Service: DemoController.getAll() inicializado.");
		List<ServiceDTO> listServices = new ArrayList<ServiceDTO>(){
			{
				add(new ServiceDTO(
						UUID.randomUUID().toString(),
						"service1",
						DateTime.now().toString()));

				add(new ServiceDTO(
						UUID.randomUUID().toString(),
						"service2",
						DateTime.now().toString()));

				add(new ServiceDTO(
						UUID.randomUUID().toString(),
						"service3",
						DateTime.now().toString()));
			}
		};
		return ResponseEntity.ok( new Response(listServices) );
	}
}
