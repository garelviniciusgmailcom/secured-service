package com.service.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@ComponentScan
public class DemoSecuredApplication {
		
	public static void main(String[] args) {
		SpringApplication.run(DemoSecuredApplication.class, args);
	}

}
