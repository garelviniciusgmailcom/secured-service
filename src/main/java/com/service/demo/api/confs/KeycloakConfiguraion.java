package com.service.demo.api.confs;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.management.HttpSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@KeycloakConfiguration
public class KeycloakConfiguraion extends KeycloakWebSecurityConfigurerAdapter{

		@Autowired
	    public KeycloakClientRequestFactory keycloakClientRequestFactory;
		
	    @Autowired
	    public void configureGlobal( AuthenticationManagerBuilder auth) throws Exception {
	        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
	        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper( new SimpleAuthorityMapper() );
	        auth.authenticationProvider( keycloakAuthenticationProvider );
	    }
	    
	    @Bean
	    public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
	        return new KeycloakSpringBootConfigResolver();
	    }
	    
	    @Bean
	    @Override
	    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
	        return new RegisterSessionAuthenticationStrategy( new SessionRegistryImpl() );
	    }
	        
	    @Bean
	    @Override
	    @ConditionalOnMissingBean(HttpSessionManager.class)
	    protected HttpSessionManager httpSessionManager() {
	        return new HttpSessionManager();
	    }
	    
	    /**
	     * Secure appropriate endpoints
	     */
	    @Override
	    protected void configure(final HttpSecurity httpSecurity) throws Exception {
	    	super.configure(httpSecurity);
	        httpSecurity.csrf().disable().exceptionHandling()
	        .and()
			.sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS )
			.and()
			.authorizeRequests().antMatchers("/swagger-ui.html**").permitAll()
			.antMatchers("/error**").hasAnyRole("USER")
			.antMatchers("/api/customers/**").hasRole("USER")
			.anyRequest().permitAll();
	    }
}
