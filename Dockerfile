FROM maven:3.6.0-jdk-8-alpine
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean install -DskipTests
VOLUME /usr/local/lib/app
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "/home/app/target/secured-service-0.0.1-SNAPSHOT.jar", "-Xmx500 -Xms500"]



